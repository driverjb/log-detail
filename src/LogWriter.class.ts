import { DateTime } from 'luxon';

export interface Configuration {
  enabled?: boolean;
  appName?: string;
  level?: Level;
  timestampFormat?: string;
  timezone?: Timezone;
}
export interface KeyValueData {
  [key: string]: any;
}
export type Level = 'debug' | 'info' | 'warn' | 'error' | 'fatal';
export type Timezone = 'local' | 'utc';

const levelHierarchy: { [key: string]: number } = {
  debug: 1,
  info: 2,
  warn: 3,
  error: 4,
  fatal: 5
};

const DEFAULT_CONFIG: Configuration = {
  appName: 'LogDetail',
  enabled: true,
  level: 'info',
  timestampFormat: 'iso',
  timezone: 'local'
};

export class LogWriter {
  private static config: Configuration = DEFAULT_CONFIG;
  constructor(private namespace: string) {}
  /**
   * Convert a bunch of data into a legible log format
   * @param data Data package to format
   * @param uuid Unique identifier for requests to include in data
   * @param error Error is available to include
   * @returns The provided data as a string
   */
  private static prepareData(data: any = {}, uuid?: string, error?: Error) {
    if (uuid) data.uuid = uuid;
    if (error) data.error = error.toString();
    let o: string = '';
    for (let k in data) {
      if (data[k] !== undefined) {
        let d = data[k];
        if (typeof d == 'function') {
          let name = d.toString().split('(')[0].split(' ')[1];
          d = `[Function: ${name ? name : 'Anonymous'}]`;
        } else if (d instanceof Error) {
          d = `${d.name}: ${d.message}`;
        } else if (typeof d == 'object') {
          if (d) d = JSON.stringify(d);
        }
        o += o === '' ? `${k}='${d}'` : ` ${k}='${d}'`;
      }
    }
    return `[${o}]`;
  }
  /**
   * Determine if a log event of a given level should be output
   * @param testLevel The log level that is being proposed to be written
   * @returns Yes/No for log output of given level
   */
  private static shouldOutput(testLevel: Level): boolean {
    let configLevelValue = levelHierarchy[LogWriter.config.level];
    let testLevelValue = levelHierarchy[testLevel];
    if (!LogWriter.config.enabled) return false;
    if (configLevelValue > testLevelValue) return false;
    return true;
  }
  /**
   *
   * @param zone The given time zone information
   * @param format The given time format information
   * @returns The current timestamp based on the given parameters
   */
  private static getTimestamp(): string {
    let time = DateTime.now();
    switch (LogWriter.config.timezone) {
      case 'local':
        time = time.toLocal();
        break;
      case 'utc':
        time = time.toUTC();
        break;
      default:
        time = time.setZone(LogWriter.config.timezone);
        if (!time.isValid) time = time.toUTC();
    }
    switch (LogWriter.config.timestampFormat) {
      case 'iso':
        return time.toISO();
      default:
        return time.toFormat(LogWriter.config.timestampFormat);
    }
  }
  /**
   * Update the current global log writer configuration
   * @param config The configuration settings to use
   */
  public static updateConfig(config: Configuration) {
    LogWriter.config = config;
  }
  /**
   * Write a debug log entry. Used for the utmost details of system operation.
   * @param message The message to write
   * @param args A collection of args. Can be string for uuid, Error (or extended Error), or KeyValueData object
   */
  public debug(message: string, ...args: Array<string | Error | KeyValueData>) {
    this.write('debug', message, ...args);
  }
  /**
   * Write an info log entry. Used for logging normal service operation.
   * @param message The message to write
   * @param args A collection of args. Can be string for uuid, Error (or extended Error), or KeyValueData object
   */
  public info(message: string, ...args: Array<string | Error | KeyValueData>) {
    this.write('info', message, ...args);
  }
  /**
   * Write a warning log entry. Used for logging events that represent unexpected server
   * behavior which is being recovered from.
   * @param message The message to write
   * @param args A collection of args. Can be string for uuid, Error (or extended Error), or KeyValueData object
   */
  public warn(message: string, ...args: Array<string | Error | KeyValueData>) {
    this.write('warn', message, ...args);
  }
  /**
   * Write an error log entry. Used for logging errors that are fatal to the operation.
   * @param message The message to write
   * @param args A collection of args. Can be string for uuid, Error (or extended Error), or KeyValueData object
   */
  public error(message: string, ...args: Array<string | Error | KeyValueData>) {
    this.write('error', message, ...args);
  }
  /**
   * Write a fatal log entry. Used for logging errors that are fatal to the service.
   * @param message The message to write
   * @param args A collection of args. Can be string for uuid, Error (or extended Error), or KeyValueData object
   */
  public fatal(message: string, ...args: Array<string | Error | KeyValueData>) {
    this.write('fatal', message, ...args);
  }
  private write(
    level: Level,
    message: string,
    ...args: Array<string | Error | KeyValueData>
  ): void {
    if (LogWriter.shouldOutput(level)) {
      let timestamp = LogWriter.getTimestamp();
      let opt: any = {};
      args.forEach((arg) => {
        if (typeof arg == 'string') opt.uuid = arg;
        else if (arg instanceof Error) opt.error = arg;
        else opt.data = arg;
      });
      let dataString = LogWriter.prepareData(opt.data, opt.uuid, opt.error);
      let out = `${timestamp} ${LogWriter.config.appName} ${this.namespace} ${level} ${dataString} ${message}`;
      console.log(out);
    }
  }
}
